import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EmpleadosComponent } from './empleados/empleados.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { NgModule } from '@angular/core';



@NgModule({
  declarations: [
    EmpleadosComponent,
    EmpleadoComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    EmpleadosComponent,
    EmpleadoComponent
  ]
})
export class ComponentsModule { }
