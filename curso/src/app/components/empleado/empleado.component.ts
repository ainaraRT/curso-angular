import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  nombre: string = "Ainara"
  apellido: string = "Ramos"
  edad: number = 22
  // empresa: string = "Athagon"
  empresa: string = ""
  private estadoEmpleado: string = "Prácticas"
  habilitacionCuadro = false;
  usuRegistrado = false;
  textoDeRegistro = "No hay nadie registrado.";

  constructor() { }

  ngOnInit(): void {
  }

  getEstado(){
    return this.estadoEmpleado;
  }

  //no hace nada, se limita a recibir el parámetro
  llamaEmpresa(value: String){}

  getRegistroUsuario() {
    this.usuRegistrado = false;
  }

  setUsuarioRegistrado(event: Event) {
    // alert("El usuario se acaba de registrar");
    // this.textoDeRegistro = "El usuario se acaba de registrar";
    // alert(event.target)
    if ((<HTMLInputElement>event.target).value == "si") {
      this.textoDeRegistro = "El usuario se acaba de registrar";
    } else {
      this.textoDeRegistro = "No hay nadie registrado.";
    }
  }

  // cambiaEmpresa(event: Event) {
  //   this.empresa = (<HTMLInputElement>event.target).value;
  // }

}
