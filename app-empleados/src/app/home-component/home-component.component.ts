import { EmpleadosService } from './../empleados.service';
import { Empleado } from './../empleado.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.css']
})
export class HomeComponentComponent implements OnInit {
  titulo = 'Listado de Empleados';

  empleados: Empleado[] = [];

  cuadroNombre: string = "";
  cuadroApellido: string = "";
  cuadroCargo: string = "";
  cuadroSalario: number = 0;

  constructor(
    private empleadosSerice: EmpleadosService
  ) { }

  ngOnInit(): void {
    // this.empleados = this.empleadosSerice.empleados;
    // console.log(this.empleadosSerice.obtenerEmpleados());
    this.empleadosSerice.obtenerEmpleados()
    .subscribe(
      misEmpleados =>{
        console.log(misEmpleados);
        this.empleados = Object.values(misEmpleados);
        this.empleadosSerice.setEmpleados(this.empleados);
      });
  }

  agregarEmpleado() {
    let miEmpleado = new Empleado(
      this.cuadroNombre,
      this.cuadroApellido,
      this.cuadroCargo,
      this.cuadroSalario
    );
    // this.empleados.push(miEmpleado);
    // this.miServicio.muestraMensaje("Nombre del empleado: " + miEmpleado.nombre);
    this.empleadosSerice.agregarEmpleadoServicio(miEmpleado);
  }
}
