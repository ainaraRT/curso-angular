import { EmpleadosService } from './../empleados.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Empleado } from './../empleado.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-actualiza-component',
  templateUrl: './actualiza-component.component.html',
  styleUrls: ['./actualiza-component.component.css']
})
export class ActualizaComponentComponent implements OnInit {

  cuadroNombre: string = "";
  cuadroApellido: string = "";
  cuadroCargo: string = "";
  cuadroSalario: number = 0;

  empleados: Empleado[] = [];

  indice: number;

  accion: number;

  constructor(
    private router: Router,
    private empleadosSerice: EmpleadosService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.empleados = this.empleadosSerice.empleados;
    this.indice = this.route.snapshot.params['id'];

    let empleado: Empleado = this.empleadosSerice.encontrarEmpleado(this.indice);

    this.cuadroNombre = empleado.nombre;
    this.cuadroApellido = empleado.apellido;
    this.cuadroCargo = empleado.cargo;
    this.cuadroSalario = empleado.salario;

    this.accion = parseInt(this.route.snapshot.queryParams['accion']);
  }

  // actualizaEmpleado() {
  //   let miEmpleado = new Empleado(
  //     this.cuadroNombre,
  //     this.cuadroApellido,
  //     this.cuadroCargo,
  //     this.cuadroSalario
  //   );
  //   // this.empleados.push(miEmpleado);
  //   // this.miServicio.muestraMensaje("Nombre del empleado: " + miEmpleado.nombre);
  //   this.empleadosSerice.actualizarEmpleado(this.indice, miEmpleado);
  //   this.router.navigate(['']);
  // }

  // eliminaEmpleado() {
  //   this.empleadosSerice.eliminarEmpleado(this.indice);
  //   this.router.navigate(['']);
  // }

  actualizaEmpleado() {
    if(this.accion == 1) {
      let miEmpleado = new Empleado(
        this.cuadroNombre,
        this.cuadroApellido,
        this.cuadroCargo,
        this.cuadroSalario
      );
      // this.empleados.push(miEmpleado);
      // this.miServicio.muestraMensaje("Nombre del empleado: " + miEmpleado.nombre);
      this.empleadosSerice.actualizarEmpleado(this.indice, miEmpleado);
      this.router.navigate(['']);
    } else {
      this.empleadosSerice.eliminarEmpleado(this.indice);
      this.router.navigate(['']);
    }
  }

  volverHome() {
    this.router.navigate(['']);
  }

}
