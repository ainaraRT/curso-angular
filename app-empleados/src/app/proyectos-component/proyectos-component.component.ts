import { Empleado } from './../empleado.model';
import { EmpleadosService } from './../empleados.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-proyectos-component',
  templateUrl: './proyectos-component.component.html',
  styleUrls: ['./proyectos-component.component.css']
})
export class ProyectosComponentComponent implements OnInit {

  cuadroNombre: string = "";
  cuadroApellido: string = "";
  cuadroCargo: string = "";
  cuadroSalario: number = 0;

  empleados: Empleado[] = [];

  constructor(
    private router: Router,
    private empleadosSerice: EmpleadosService
  ) { }

  ngOnInit(): void {
    this.empleados = this.empleadosSerice.empleados;
  }

  agregarEmpleado() {
    let miEmpleado = new Empleado(
      this.cuadroNombre,
      this.cuadroApellido,
      this.cuadroCargo,
      this.cuadroSalario
    );
    // this.empleados.push(miEmpleado);
    // this.miServicio.muestraMensaje("Nombre del empleado: " + miEmpleado.nombre);
    this.empleadosSerice.agregarEmpleadoServicio(miEmpleado);
    this.router.navigate(['']);
  }

  volverHome() {
    this.router.navigate(['']);
  }
}
