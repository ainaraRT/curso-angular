import { Empleado } from './empleado.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private httpClient: HttpClient
  ) { }

  cargarEmpleados() {
    return this.httpClient.get('https://mis-clientes-3bb33-default-rtdb.europe-west1.firebasedatabase.app/datos.json');
  }

  guardarEmpleados(empleados: Empleado[]){
    this.httpClient
    .put('https://mis-clientes-3bb33-default-rtdb.europe-west1.firebasedatabase.app/datos.json', empleados)
    .subscribe(
      response => console.log("Se han guardado los empleados: " + response),
      error => console.log("Error: " + error)
      );
  }
}
