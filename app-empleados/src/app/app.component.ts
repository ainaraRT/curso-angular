import { EmpleadosService } from './empleados.service';
import { ServicioEmpleadosService } from './servicio-empleados.service';
import { Empleado } from './empleado.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  titulo = 'Listado de Empleados';

  // empleados: Empleado[] = [
  //   new Empleado("Paula", "Usero", "Presidenta", 7500),
  //   new Empleado("Ana", "Martín", "Directora", 5500),
  //   new Empleado("María", "Fernández", "Jefa Sección", 3500),
  //   new Empleado("Laura", "López", "Administrativo", 2500),
  // ];

  empleados: Empleado[] = [];

  cuadroNombre: string = "";
  cuadroApellido: string = "";
  cuadroCargo: string = "";
  cuadroSalario: number = 0;

  constructor(
    // private miServicio: ServicioEmpleadosService,
    private empleadosSerice: EmpleadosService
  ) {
    // this.empleados = this.empleadosSerice.empleados;
  }
  ngOnInit(): void {
    this.empleados = this.empleadosSerice.empleados;
  }

  agregarEmpleado() {
    let miEmpleado = new Empleado(
      this.cuadroNombre,
      this.cuadroApellido,
      this.cuadroCargo,
      this.cuadroSalario
    );
    // this.empleados.push(miEmpleado);
    // this.miServicio.muestraMensaje("Nombre del empleado: " + miEmpleado.nombre);
    this.empleadosSerice.agregarEmpleadoServicio(miEmpleado);
  }
}
