import { DataService } from './data.service';
import { ServicioEmpleadosService } from './servicio-empleados.service';
import { Empleado } from './empleado.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  // empleados: Empleado[] = [
  //   new Empleado("Paula", "Usero", "Presidenta", 7500),
  //   new Empleado("Ana", "Martín", "Directora", 5500),
  //   new Empleado("María", "Fernández", "Jefa Sección", 3500),
  //   new Empleado("Laura", "López", "Administrativo", 2500),
  // ];

  empleados: Empleado[] = [];

  constructor(
    private servicioAlert: ServicioEmpleadosService,
    private dataService: DataService
  ) { }

  setEmpleados(misEmpleados: Empleado[]) {
    this.empleados = misEmpleados;
  }

  obtenerEmpleados() {
    return this.dataService.cargarEmpleados();
  }

  agregarEmpleadoServicio(empleado: Empleado) {
    this.servicioAlert.muestraMensaje("Persona que se va a agregar: " + "\n" + empleado.nombre + "\n" + "Salario: " + empleado.salario);
    this.empleados.push(empleado);
    this.dataService.guardarEmpleados(this.empleados);
  }

  encontrarEmpleado(indice: number) {
    let empleado: Empleado = this.empleados[indice];
    return empleado;
  }

  actualizarEmpleado(indice: number, empleado: Empleado) {
    let empleadoModificado = this.empleados[indice];

    empleadoModificado.nombre = empleado.nombre;
    empleadoModificado.apellido = empleado.apellido;
    empleadoModificado.cargo = empleado.cargo;
    empleadoModificado.salario = empleado.salario;
  }

  eliminarEmpleado(indice: number) {
    this.empleados.splice(indice, 1);
  }
}
