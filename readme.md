# Curso Angular

Curso sobre cómo utilizar Angular en [YouTube](https://www.youtube.com/playlist?list=PLU8oAlHdN5BnNAe8zXnuBNzKID39DUwcO)

## Iniciar proyecto en Angular
```
PS C:\Users\cursoAngular> ng new cursoAngular --directory ./
¡¡¡ --directory ./ -> Por si se quiere en la misma carpeta en la que se trabaja
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? CSS
```


- Para iniciar el proyecto: ng serve -o
- Hacer commit cada vez que está estable y arranca
- Eliminar todo el contenido de app.component.html, dejando SOLAMENTE: <router-outlet></router-outlet>
¡¡COMMIT


- Creamos los módulos dentro de las carpetas de: cd src/app/
```
PS > ng g module components
     ng g module guards    
     ng g module interceptors
     ng g module services
```

- Creamos una carpeta para los módulos dentro de src/app: mkdir model
PS > cd components
¡¡COMMIT



- Creamos el componente de las pantallas: ng g component images|home|login
¡¡ se crea el html, css, ts y spec.ts del componente imágenes, ejemplo: images.compoment.html
¡¡COMMIT



- Para generar servicios:
```
PS > cd src/app/services/
PS > ng g service Login
CREATE src/app/services/login.service.spec.ts (352 bytes)
CREATE src/app/services/login.service.ts (134 bytes)
```



!!!! Los interceptors se utilizan junto con el módulo http para interceptar absolutamente todo lo que pase por el módulo http. Nos vale por ejemplo, si tenemos una pieza de software que todo lo que pase por http, va a pasar por ella, pues es el sitio idóneo para que yo esté escuchando a ver si tenemos un usuario logado y si lo tenemos, le pasamos el tóken. De la misma forma que es el sitio perfecto para tener una línea de log y siempre que hacemos una petición http, que pase por ahí.



- Creamos los typescripts de los interceptores
```
PS > cd src/app/interceptors
PS > ng g interceptor authHttp
CREATE src/app/interceptors/auth-http.interceptor.spec.ts (429 bytes)
CREATE src/app/interceptors/auth-http.interceptor.ts (413 bytes)
```
- Ponemos los datos en AuthInterceptor
- En AppModule, antes de bootstrap: [AppComponent]:
     providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true}]
¡¡COMMIT



- Creamos los guards
```
PS > cd src/app/guards
PS > ng g guard authguard
? Which interfaces would you like to implement? (Press <space> to select, <a> to toggle all, <i> to invert selection)
>(*) CanActivate
 ( ) CanActivateChild
 ( ) CanDeactivate
 ( ) CanLoad
? Which interfaces would you like to implement? CanActivate
CREATE src/app/guards/authguard.guard.spec.ts (356 bytes)
CREATE src/app/guards/authguard.guard.ts (462 bytes)
```

- Ponemos los datos en AuthGuard
- En AppRouting, debajo de: component: HomeComponent
     canActivate: [AuthguardGuard]
¡¡COMMIT

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ainaraRT/curso-angular.git
git branch -M main
git push -uf origin main
```


## Crear un proyecto con Firebase
[Firebase](https://firebase.google.com/?hl=es-419)
[Mis clientes Firebase Proyecto](https://console.firebase.google.com/u/0/project/mis-clientes-3bb33/overview?hl=es-419)
(Url base de datos)[https://mis-clientes-3bb33-default-rtdb.europe-west1.firebasedatabase.app/]


## Añadir Tailwind a un proyecto de Angular

[Página](https://dev.to/angular/setup-tailwindcss-in-angular-the-easy-way-1i5l)
Vídeo explicativo en [Youtube](https://www.youtube.com/watch?v=msJmj3Fz4OU&ab_channel=DominiCode)

- Instalar Tailwind
```
PS C:\Users\cursoAngular\cursoDirectivas> npm install -D tailwindcss
npm WARN ERESOLVE overriding peer dependency
npm WARN While resolving: curso-directivas@0.0.0
npm WARN Found: tailwindcss@3.0.24
npm WARN node_modules/tailwindcss
npm WARN   dev tailwindcss@"latest" from the root project
npm WARN   2 more (@tailwindcss/forms, @tailwindcss/typography)
npm WARN 
npm WARN Could not resolve dependency:
npm WARN peerOptional tailwindcss@"^2.0.0" from @angular-devkit/build-angular@12.2.17
npm WARN node_modules/@angular-devkit/build-angular
npm WARN   dev @angular-devkit/build-angular@"~12.2.16" from the root project

added 28 packages, removed 1 package, and audited 1327 packages in 8s

103 packages are looking for funding
  run `npm fund` for details

7 high severity vulnerabilities

To address all issues (including breaking changes), run:
  npm audit fix --force

Run `npm audit` for details.
```

- (Opcional) Tailwind Plugins
```
npm i @tailwindcss/typography

npm i @tailwindcss/forms
```

- Cambiar en el archivo styles.css: 
```
@tailwind base;
@tailwind components;
@tailwind utilities;
```
